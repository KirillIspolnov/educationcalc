package net.kivitechnologies.BrainWork.Calc;

import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Класс, выполняющий основную работу калькулятора - ввод данных, вывод результатов на экран
 * Реализовано отслеживание ошибок пользователя:
 * <ul>
 * <li>деление на нуль</li>
 * <li>ввод знака неподдерживаемой операции</li>
 * </ul>
 * Также реализована возможность продолжить вычисления с новыми исходными данными без перезапуска программы 
 * 
 * @version 2.1
 * @author К.В. Испольнов, KiVI, 16ит18К
 */

/**
 * @TODO Реализовать распознавание, парсинг и решение вложенных выражений
 * @TODO Добавить возможность выбора в случае ошибки в работе программы:
 * либо ввести все выражение снова
 * либо ввести неправильный сегмент выражения
 * либо завершить использование программы 
 * 
 * @author К.В. Испольнов, KiVI, 16ит18К
 *
 */
public class CalcEngine {
	/**
	 * Строковые константы, используемые для вывода информационных сообщений пользователю
	 * Однажды, я избавлюсь от них и добавлю многоязычность
	 */
	public static final String ERROR_WITH_OPERATION_SIGN          = "Хм, похоже, я пока не знаю этой операции. Может, попробуете ввести выражение еще раз (y/n)?",
		           	   ERROR_DIVIDING_BY_ZERO             = "Когда меня создавали, делить на ноль было запрещено. Разве что-то изменилось?! Может, попробуете ввести выражение еще раз (y/n)?",
				   ERROR_UNKNOWN_ERROR_AT_PARSING_1   = "Хм, похоже, при обработке первого операнда произошла какая-то ошибка. Попробуете ввести выражение заново?",
				   ERROR_UNKNOWN_ERROR_AT_PARSING_2   = "Хм, похоже, при обработке второго операнда произошла какая-то ошибка. Попробуете ввести выражение заново?",
				   MESSAGE_GOOD_BYE                   = "Спасибо за использование калькулятора! До свидания!",
				   MESSAGE_GIVE_ME_EXP                = "Введите выражение: ",
				   MESSAGE_WOULD_YOU_LIKE_TO_CONTINUE = "Хотите выполнить еще какие-то расчеты (y/n)?";
	/**
	 * Скрытые переменные, используемые в работе класса:
	 * <ul>
	 * <li>Scanner scanner - используется для ввода данных с клавиатуры</li>
	 * <li>Number firstOperand - используется для хранения первого операнда</li>
	 * <li>Number secondOperand - используется для хранения второго операнда</li>
	 * <li>String operation - хранит знак операции, а таке используется как флаг для определения введен ли первый операнд</li>
	 * </ul>					   
	 */
	private Scanner scanner;
	private Number firstOperand, secondOperand;
	private String operation = "";
	
	/**
	 * Конструктор класса
	 * 
	 * @param yourScanner Сканнер для ввода данных с клавиатуры, созданный в коде другого класса
	 */
	public CalcEngine(Scanner yourScanner) {
		this.scanner = yourScanner;
	}
	
	/**
	 * Метод считывает строку с клавиатуры, содержащую два числа и знак операции
	 * Метод разделяет строку на отдельные символы, затем перебирая символы, метод определяет, является ли символ числом, точкой или запятой с помощью регулярного выражения
	 * Если символ не является числом, точкой или запятой, то символ конкатенируется с переменной для хранения знака операции
	 * Иначе, символ конкатенируется с переменной для хранения первого или второго операнда в виде строки
	 * 
	 * Затем, метод сравнивает является ли второй операнд нулем, а знак операции соответствует знаку деления
	 * Если это так, метод предлагает пользователю ввести выражения снова 
	 * 
	 * Update 13-11-17: Объявлен устаревшим в связи с необходимостью увеличить функционал, но при этом сохранить этот код в назидание потомкам
	 */
	@Deprecated
	public void readData() {
		String line = scanner.nextLine().replace(" ", "");
		String[] symbols = line.split("");
		
		String first = "", second = "";
		
		for(int i = 0; i < line.length(); i++) {
			if(symbols[i].equals("-")) {
				if(first.equals(""))
					first = "-";
				else if(second.equals(""))
					second = "-";
				
				continue;
			} 
			
			if(!Pattern.matches("[0-9]|\\.|,", symbols[i])) {
				operation += symbols[i];
			} else {
				if(symbols[i].equals(","))
					symbols[i] = ".";
				
				if(operation == "")
					first += symbols[i];
				else
					second += symbols[i];
			}
		}
		
		firstOperand = validate(Double.valueOf(first));
		secondOperand = validate(Double.valueOf(second));
		
		if((operation.equals("/") || operation.equals("\\") || operation.equals("div")) && secondOperand.equals(0))
			relaunch(ERROR_DIVIDING_BY_ZERO);
	}
	
	/**
	 * Выполняет математическую операцию над двумя числами, которые хранятся как поля класса firstOperand и secondOperand
	 */
	public void execute() {
		Number result = null;
		
		if(operation.equals("+"))
			result = sum();
		else if(operation.equals("-"))
			result = difference();
		else if(operation.equals("*"))
			result = multiple();
		else if(operation.equals("/") || operation.equals("\\"))
			result = divide();
		else if(operation.equals("div"))
			result = divide().intValue();
		else if(operation.equals("%"))
			result = surplus();
		else if(operation.equals("^"))
			result = power();
		else 
			relaunch(ERROR_WITH_OPERATION_SIGN);
				
		//Переменные для хранения строк форматирования, используемых в выводе результатов
		String firstOperandFormat = "%.3f", 
			   secondOperandFormat = "%.3f",
			   resultFormat = "%.3f";
				
		if(firstOperand instanceof Integer)
			firstOperandFormat = "%d";
		if(secondOperand instanceof Integer)
			secondOperandFormat = "%d";
		if(result instanceof Integer)
			resultFormat = "%d";
		
		//Заворачиваем выводимое число в скобки, если оно меньше нуля
		if(secondOperand.doubleValue() < 0)
			secondOperandFormat = "(" + secondOperandFormat + ")";
		
		System.out.printf(firstOperandFormat + " %s " + secondOperandFormat + " = " + resultFormat + "%n", firstOperand, operation, secondOperand, result);
		
		relaunch(MESSAGE_WOULD_YOU_LIKE_TO_CONTINUE);
	}
	
	/**
	 * Метод считывает строку с клавиатуры, содержащую два числа и знак операции
	 * Затем метод начинает перебирать символы строки, пока не встретит знак операции
	 * Как только такой знак найден, выполнение первого цикла прерываается и метод сохраняет уже прочитанный кусок
	 * строки как первый операнд
	 * Затем метод получает подстроку длиной в три символа, если она имеет вид div, это означает
	 * что пользователь желает произвести целочисленное деление
	 * В любом другом случае запоминается символ с индексом i как знак операции
	 * Остальная часть строки признается методом как второй операнд
	 * 
	 * Затем происходят попытки приведения строки к числу с помощью метода Double.valueOf
	 * Если происходит какая-то ошибка, связанная с написанием числа, программа уведомит об этом пользователя 
	 * и предложит ввести выражеие снова
	 */
	public void initData() {
		String line = scanner.nextLine().replace(" ", "").replaceAll("[()]", "");
		String stringifiedFirstOperand = "", stringifiedSecondOperand = "";
		int i = 0, j;
		
		if(line.startsWith("-")) {
			i++;
			stringifiedFirstOperand = "-";
		}
		
		while(!Pattern.matches("\\^|\\+|-|\\*|/|\\|%", line.substring(i, i + 1)))
			i++;
		
		stringifiedFirstOperand = line.substring(0, i);
		j = i;
		
		if(line.length() > i + 3 && line.substring(i, i + 3).equals("div")) {
			operation = "div";
			i+=3;
		} else {
			operation = line.substring(i, i + 1);
			i++;
		}
		
		operation = line.substring(j, i);
		stringifiedSecondOperand = line.substring(i);
		
		try {
			firstOperand = validate(Double.parseDouble(stringifiedFirstOperand));
		} catch(NumberFormatException nfe) {
			relaunch(ERROR_UNKNOWN_ERROR_AT_PARSING_1);
		}
		
		try {
			secondOperand = validate(Double.parseDouble(stringifiedSecondOperand));
		} catch(NumberFormatException nfe) {
			relaunch(ERROR_UNKNOWN_ERROR_AT_PARSING_2);
		}
		
		if(Pattern.matches("/|\\|div", operation) && secondOperand.equals(0)) {
			relaunch(ERROR_DIVIDING_BY_ZERO);
		}
	}
	
	/**
	 * Находит сумму двух чисел, хранящихся в переменным firstOperand и secondOperand
	 * 
	 * @return число, равное firstOperand + secondOperand
	 */
	private Number sum() {
		return validate(firstOperand.doubleValue() + secondOperand.doubleValue());
	}
	
	/**
	 * Находит разность двух чисел, хранящихся в переменным firstOperand и secondOperand
	 * 
	 * @return число, равное firstOperand - secondOperand
	 */
	private Number difference() {
		return validate(firstOperand.doubleValue() - secondOperand.doubleValue());
	}
	
	/**
	 * Находит произведение двух чисел, хранящихся в переменным firstOperand и secondOperand
	 * 
	 * @return число, равное firstOperand * secondOperand
	 */
	private Number multiple() {
		return validate(firstOperand.doubleValue() * secondOperand.doubleValue());
	}
	
	/**
	 * Находит частное двух чисел, хранящихся в переменным firstOperand и secondOperand
	 * 
	 * @return число, равное firstOperand / secondOperand
	 */
	private Number divide() {
		return validate(firstOperand.doubleValue() / secondOperand.doubleValue());
	}
	
	/**
	 * Находит число firstOperand в степени secondOperand
	 * 
	 * @return число, равное firstOperand^secondOperand
	 */
	private Number power() {
		if(secondOperand.equals(0))
			return 1;
		
		double n = firstOperand.doubleValue();
		int degree = secondOperand.intValue();
		for(int i = 1; i < Math.abs(degree); i++) {
			n *= firstOperand.doubleValue();
		}
		
		if(degree < 0)
			n = 1 / n;
		
		return validate(n);
	}
	
	/**
	 * Находит остаток от деления двух числе, хранящихся в переменных firstOperand и secondOperand
	 * 
	 * @return число, равное firstOperand % secondOperand
	 */
	private Number surplus() {
		return validate(firstOperand.intValue() % secondOperand.intValue());
	}
	
	/**
	 * Проводит валидацию числа
	 * Если остаток от деления на единицу, т.е. дробная часть числа равна 0, то метод приводит исходное число к типу int
	 * В ином случае возвращает само число
	 * 
	 * @param smth исходное число, подлежащее валидации
	 * @return число smth, приведенное к типу int, если это число действительно целое
	 */
	private Number validate(Number smth) {
		if(smth.doubleValue() % 1 == 0)
			return smth.intValue();
		return smth;
	}
	
	/**
	 * Метод позволяет продолжить вычисления без необходимости перезапуска программы
	 *  
	 * @param message информационное сообщение для вывода на экран
	 */
	private void relaunch(String message) {
		operation = "";
		firstOperand = null;
		secondOperand = null;
		System.out.println(message);
		String response = scanner.nextLine().toLowerCase();
		if(response.equals("n"))
			exit();
		else {
			System.out.print(MESSAGE_GIVE_ME_EXP);
			initData();
			execute();
		}
	}
	
	/**
	 * Останавливает выполнение программы
	 */
	private void exit() {
		System.exit(0);
	}
}
