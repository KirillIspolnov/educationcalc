package net.kivitechnologies.BrainWork.Calc;

import java.util.Scanner;

/**
 * Класс демонстрирующий работу калькулятора (класса CalcEngine)
 * 
 * @author К.В. Испольнов, KiVI, 16ит18К
 */
public class CalcDemo {
	public static final String MESSAGE_WELCOME = "Здравствуйте! Вас приветствует лучший калькулятор Вселенной! На данный момент с моей помощью Вы сможете выполнить эти действия:\n\t* Сложение\n\t* Вычитание\n\t* Умножение\n\t* Деление\n\t* Возведение в степень\n\t* Нахождение остатка от деления\nВнимание! К содалению, я не пока не поддерживаю вложенные выражения, заключенные в скобки, а также несколько операций в одном выражении\nДля начала, введите выражение:";
	
	/**
	 * Основной метод, вызываемый JVM при запуске программы
	 * 
	 * @param args массив аргументов, переданных в командной строке
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		CalcEngine myEngine = new CalcEngine(scanner);
		
		System.out.println(MESSAGE_WELCOME);
		/*myEngine.readData();*/
		myEngine.initData();
		myEngine.execute();
		
		scanner.close();
	}
}
